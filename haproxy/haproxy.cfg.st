defaults
  timeout connect 5000
  timeout client  50000
  timeout server  50000  

frontend http
  bind *:80
  
  acl service_1_path path /service-1
  use_backend service-first if service_1_path
  
  acl service_2_path path /service-2
  use_backend service-second if service_2_path
  
  default_backend service-default

backend service-first
  mode http
  http-request set-uri %[url,regsub(^/service-1,/,)] if { path /service-1 }  
  server first service-first:80

backend service-second
  mode http
  http-request set-uri %[url,regsub(^/service-2,/,)] if { path /service-2 }  
  server second service-second:80

backend service-default
  server def_service default-service:80

