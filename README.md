Hello Swedbank!

This repository is for you. This is my implementation of your Docker homework.

## Where is result ?
* http://swed.kyberorg.eu:8080 - Default Service
* http://swed.kyberorg.eu:8080/service-1 - Service 1
* http://swed.kyberorg.eu:8080/service-2 - Service 2
* http://swed.kyberorg.eu:8282/ - also Service 2 (Direct access)

For all other URLs http://swed.kyberorg.eu - will reply with empty response
